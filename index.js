const Web3 = require('web3');
var fs = require('fs');
var csv = require('fast-csv');

// replace YOUR_API_KEY with a real one
const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/YOUR_API_KEY'));

// ERC-20 smart contract should contain some logic for sending/minting tokens to the batch of addresses
// in this particular case smart contract has a function as follows:
// function mint(address[] to) public onlyOwner
const contractABI = [ { "constant": true, "inputs": [], "name": "val", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "owner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "to", "type": "address[]" } ], "name": "mint", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "recepient", "type": "address" }, { "indexed": false, "name": "amount", "type": "uint256" } ], "name": "Minted", "type": "event" } ];
const contractAddress = "0x0000000000000000000000000000000000000000";
const contract = new web3.eth.Contract(contractABI, contractAddress);

// replace with credentials for the account-owner (see Open Zeppelin's Ownable pattern) of ERC-20 smart contract
// obviously this account should have enough ERC-20 tokens and ETH for performing the airdrop
const ownerWalletAddress = '0x0000000000000000000000000000000000000000';
const ownerWalletPrivateKey = '0x0000000000000000000000000000000000000000000000000000000000000000';

// in WEI, 0.01 ETH
const ETH_VALUE = "10000000000000000";
// optimal batch size, further increasing may lead to function running out of gas
const BATCH_SIZE = 80;  

let distribData = new Array();
let allocData = new Array();

function processAirdrop() 
{
    var stream = fs.createReadStream("./airdrop.csv");
    let index = 0;

    console.log('Parsing the CSV file...');
    var csvStream = csv()
        .on("data", function(data)
        {
            let isAddress = web3.utils.isAddress(data[0]);
            if(isAddress && data[0] != null && data[0] != '')
            {
                allocData.push(data[0]);
                index++;
                if(index >= BATCH_SIZE)
                {
                    distribData.push(allocData);
                    allocData = [];
                    index = 0;
                }
            }
        })
        .on("end", function()
        {
            distribData.push(allocData);
            allocData = [];
            sendTransactions();
        });
    stream.pipe(csvStream);
}

async function sendTransactions()
{
    var txCount = await web3.eth.getTransactionCount(ownerWalletAddress);
    for (var i = 0; i < distribData.length; i++)
    {
        var contractFunction = contract.methods.mint(distribData[i]);
        var encodedABI = contractFunction.encodeABI();
        
        var tx = 
        {
            nonce: txCount++,
            from: ownerWalletAddress,
            to: contractAddress,
            data: encodedABI,
            gasLimit: 500000,
        };

        console.log("Sending tokens to accounts:\n", distribData[i]);
        web3.eth.accounts.signTransaction(tx, ownerWalletPrivateKey).then(signed => {
            web3.eth.sendSignedTransaction(signed.rawTransaction)
                .on('receipt', (receipt) => 
                { 
                    console.log("Sent tokens successfully.", receipt.gasUsed, "gas used.");
                })
                .on('error', (error) => 
                { 
                    console.log("ERROR sending tokens:", error);
                })
        });

        console.log("Sending ETH to the same accounts...");
        for(var j = 0; j < distribData[i].length; j++)
        {
            var txETH = 
            {
                nonce: txCount++,
                from: ownerWalletAddress,
                to: distribData[i][j],
                gasLimit: 500000,
                value: web3.utils.fromWei(ETH_VALUE, 'wei')
            };

            console.log("Sending ETH to", distribData[i][j], "...");
            web3.eth.accounts.signTransaction(txETH, ownerWalletPrivateKey).then(signed => {
                web3.eth.sendSignedTransaction(signed.rawTransaction)
                    .on('receipt', (receipt) => 
                    { 
                        console.log("Sent", ETH_VALUE, "ETH successfully to", distribData[i][j], ".", receipt.gasUsed, "gas used.");
                    })
                    .on('error', (error) => 
                    { 
                        console.log("ERROR sending ETH to", distribData[i][j], ":", error);
                    })
            });
        }
    }
}

console.log("Processing the airdrop. Batch size is", BATCH_SIZE, "accounts per transaction...");
processAirdrop();