This NodeJS application performs the _automated airdrop_ of ERC-20 tokens to the batches of addresses from a CSV file. It also sends some small amounts of ETH to each address (yeah, we all do weird stuff for our customer's needs sometimes).

Addresses are stored in ./airdrop.csv.

Following instructions from comments in ./index.js and replacing some custom stuff in variables section is required **before** running.

Start with:
- npm install
- node index.js